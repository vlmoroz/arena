/**

* Задание - Трансформеры и ООП

*

* 1. Создать класс Transformer со свойствами name и health (по умолчанию

* имеет значение 100) и методом attack()

* 2. Создать класс Autobot, который наследуется от класса Transformer.

* - Имеет свойсто weapon, т.к. автоботы сражаются с использованием оружия.

* - Конструктор класса принимает 2 параметра: имя и оружее (Экземпляр класса

* Weapon).

* - Метод attack возвращает результат использования оружия weapon.fight()

* 3. Создать класс Deceptikon который наследуется от класса Transformer.

* - Десептиконы не пользуются оружием, поэтому у них нет свойства weapon. Зато

* они могут иметь разное количество здоровья.

* - Конструктор класса принимает 2 параметра: имя и количество здоровья health

* - Метод attack возвращает характеристики стандартного вооружения: { damage: 5, speed: 1000 }

* 4. Создать класс оружия Weapon, на вход принимает 2 параметра: damage - урон

* и speed - скорость атаки. Имеет 1 метод fight, который возвразает характеристики

* оружия в виде { damage: 5, speed: 1000 }

* 5. Создать 1 автобота с именем OptimusPrime с оружием, имеющим характеристики { damage: 100, speed: 1000 }

* 6. Создать 1 десептикона с именем Megatron и показателем здоровья 10000

* 7. Посмотреть что происходит при вызове метода atack() у траснформеров разного типа,

* посмотреть сигнатуры классов

* 8. ДЗ-Вопрос: сколько нужно автоботов чтобы победить Мегатрона если параметр speed в оружии это

* количество милсекунд до следующего удара? Реализовать симуляцию боя.

*/



class Transformer {
	constructor(name, health = 100){
		this.name = name;
		this.health = health;
	}
	attack(){}

	hit(weapon){
		return this.health = this.health - weapon.damage;
	}
}

class Autobot extends Transformer{
	constructor(name, weapon, health = 100){
		super(name);
		this.health = health;
		this.weapon = weapon;
	}

	attack(){
		return this.weapon.fight()
	}
}

class Deceptikon extends Transformer{
	attack(){
		return {damage: 5, speed: 100}
	}
}

class Weapon {
	constructor(damage, speed){
		this.damage = damage;
		this.speed = speed;
	}

	fight(){
		return {
			damage: this.damage,
			speed: this.speed
		}
	}
}



// OptimusPrime
const weaOptimusPrime = new Weapon(100, 1000);
const optimusPrime = new Autobot ('Optimus Prime', weaOptimusPrime);
// OptimusPrime


// Megatron
const Megatron = new Deceptikon('Megatron',10000)
// Megatron



class Arena {
	constructor(side1, side2){
		this.side1 = side1;
		this.side2 = side2;
	}

	title = document.querySelector('h1');

	fight(ms){
		const timer = setInterval(

			() => {			
				// Сторона 1 атакует сторону 2			
				this.side1.forEach(bot => this.side2[0].hit(bot.attack()));
						
				// Проверяем условие победы стороны 1			
				this.side2 = this.side2.filter(bot => bot.health > 0);
			
				if (!this.side2.length) {		
					this.title.innerText = "Autobot wins";			
					clearInterval(timer);			
				}
					
				// Сторона 2 атакует сторону 1			
				this.side2.forEach(bot => this.side1[0].hit(bot.attack()));
						
				// Проверяем условие победы стороны 2			
				this.side1 = this.side1.filter(bot => bot.health > 0);
			
				if (!this.side1.length) {
					this.title.innerText = "Deceptikon wins";		
					console.log('Сторона 2 победила');			
					clearInterval(timer);			
				}		
			}, ms			
		);	
	}
}


class Renderer {
	constructor(side1, side2, ms){
		this.side1 = side1;
		this.side2 = side2;
		this.ms = ms;
		this.arena = new Arena(this.side1, this.side2);		
		this.htmlSide1 = document.querySelector('.arena-side-1');
		this.htmlSide2 = document.querySelector('.arena-side-2');
		this.htmlSide1.innerHTML = this.htmlSide2.innerHTML = '';		
	}

	renderSide(side, htmlSide){	
		side.forEach((el) => {
			const div = document.createElement('div');
			div.classList.add('bot');
			const hp = document.createElement('span');
			hp.innerText = el.name + ' ' + el.health + ' HP';
			div.appendChild(hp);
			if(el.health > 0) htmlSide.appendChild(div);
		})
	}

	renderArena(){
		this.htmlSide1.innerHTML = this.htmlSide2.innerHTML = '';
		this.renderSide(this.side1,this.htmlSide1);
		this.renderSide(this.side2,this.htmlSide2);
	}

	start(){
		this.arena.fight(this.ms);
        setInterval(() => this.renderArena(), this.ms);
	}
}


const autobots = 
	[
	new Autobot ('Optimus Prime', new Weapon(100,1000)),
	new Autobot ('Autobot 1', new Weapon(30,1000)),
	new Autobot ('Autobot 2', new Weapon(30,1000)),
	new Autobot ('Autobot 3', new Weapon(30,1000)),
	new Autobot ('Autobot 4', new Weapon(30,1000))
	];

const deceptikons = 
	[
	new Deceptikon('Megatron',10000)
	]

const battle = new Renderer(autobots, deceptikons, 100);

battle.start()